from datadog import api
from datadog import initialize
import json
import os
import SourceQuery
import time
import urllib

my_ip = json.loads(urllib.urlopen("http://ip.jsontest.com/").read())['ip']
server = SourceQuery.SourceQuery(my_ip, 27015)
gametracker_url = "http://cache.www.gametracker.com/server_info/luker.zzzz.io"\
        ":27015/b_160_400_1_ffffff_c5c5c5_ffffff_000000_1_1_1.png"
server_name = "Bad Dragon Co-Op Insurgency Server"

# Load api keys for talking to datadog
options = {
  'api_key': '',
  'app_key': ''
}

initialize(**options)

# Post the current number of players to the dashboard
print api.Metric.send(
  metric='Current_Players',
  points=(time.time(), server.info()['numplayers'])
)

# Write current map to a file, if it changes, post an event to datadog
map_file = '/tmp/current_map.txt'
current_map = server.info()['map']
if not os.path.exists(map_file):
  open(map_file, 'w').write(current_map)

if os.path.exists(map_file):
  previous_map = open(map_file, 'r').read().rstrip()
  if previous_map != current_map:
    open(map_file, 'w').write(current_map)
    print api.Event.create(title=current_map, text=current_map)


# Draw my dashboard
board_id = '74676'
updated_board = {
    "width": 500,
    "height": 90,
    "board_title": "%s - %s" % (server_name, my_ip),
    "widgets": [
      {
        "type": "query_value",
        # Title
        "title_text": "Current Players",
        "title_size": 16,
        "title": True,
        "title_align": "center",
        # Sizing
        "height": 15,
        "width": 20,
        # Positioning
        "y": 0,
        "x": 0,

        # Widget Parameters
        "aggregator": "last",  # Choose from: [avg, sum, min, max]
        "query": "avg:Current_Players{host:ein}",
        "conditional_formats": [
          {
            "color": "white_on_red",
            "invert": False,
            "comparator": ">=",  # Choose from: [>, >=, <, <=]
            "value": 10
          },
          {
            "color": "white_on_yellow",
            "invert": False,
            "comparator": ">=",  # Choose from: [>, >=, <, <=]
            "value": 8
          },
          {
            "color": "white_on_green",
            "invert": False,
            "comparator": "<=",  # Choose from: [>, >=, <, <=]
            "value": 5
          },
        ],
        "text_align": "center",
        "precision": 0,
        "timeframe": "1h",  # Choose from: [5m, 10m, 1h, 4h, 1d, 2d, 1w]
        "text_size": "auto",
        "unit": "auto"  # Give a custom unit or use "auto"
      },
      {
        "type": "timeseries",
        # Title
        "title": True,
        "title_size": 16,
        "title_align": "left",
        "title_text": "Player count - last day",
        # Sizing
        "height": 15,
        "width": 45,
        # Positioning
        "y": 0,
        "x": 21,
        # Widget Parameters
        "timeframe": "1d",  # Choose from: [1h, 4h, 1d, 2d, 1w]
        "tile_def": {
          "viz": "timeseries",
          "requests": [
            {
               "q": "avg:Current_Players{host:ein}"
            }
          ],
        }
      },
      {
        "type": "timeseries",
        # Title
        "title": True,
        "title_size": 16,
        "title_align": "left",
        "title_text": "Memory Used by Insurgency - last day",
        # Sizing
        "height": 15,
        "width": 45,
        # Positioning
        "y": 18,
        "x": 21,
        # Widget Parameters
        "timeframe": "1d",  # Choose from: [1h, 4h, 1d, 2d, 1w]
        "tile_def": {
          "viz": "timeseries",
          "requests": [
            {
               "q": "avg:system.processes.mem.rss{process_name:srcds}",
               "type": "area"
            }
          ],
        }
      },
      {
        "type": "timeseries",
        # Title
        "title": True,
        "title_size": 16,
        "title_align": "left",
        "title_text": "CPU Usage by Insurgency - last day",
        # Sizing
        "height": 15,
        "width": 45,
        # Positioning
        "y": 36,
        "x": 21,
        # Widget Parameters
        "timeframe": "1d",  # Choose from: [1h, 4h, 1d, 2d, 1w]
        "tile_def": {
          "viz": "timeseries",
          "requests": [
            {
               "q": "avg:system.processes.cpu.pct{process_name:srcds}",
               "type": "area"
            }
          ],
        }
      },
      {
        "type": "event_stream",
        # Title
        "title": True,
        "title_size": 16,
        "title_align": "left",
        "title_text": "Current Map",
        # Sizing
        "height": 15,
        "width": 22,
        # Positioning
        "y": 0,
        "x": 67,
        # Widgets Parameters
        "query": "*",
        "timeframe": "1h"  # Choose from: [1h, 4h, 1d, 2d, 1w]
      },
      {
        "type": "image",
        # Sizing
        "height": 49,
        "width": 19,
        # Positioning
        "y": 18,
        "x": 0,
        # Widget Parameters
        "url": gametracker_url
      }
    ]
}

# Send the actual update to our dashbaord
api.Screenboard.update(board_id, **updated_board)
